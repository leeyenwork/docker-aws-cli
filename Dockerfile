ARG NODE_VER=lts
ARG NPM_VER

FROM node:$NODE_VER-alpine
ARG TARGETARCH

ARG NODE_VER=lts
ARG NPM_VER

RUN apk add --no-cache python3 py3-pip git curl bash && \
    apk add --no-cache gcc python3-dev musl-dev libffi-dev && \
    pip3 --disable-pip-version-check install \
        boto3 botocore awscli aws-sam-cli --break-system-packages && \
    apk -v --purge --no-cache del gcc python3-dev musl-dev

# change npm version
RUN echo "NODE VERSION: $(node -v)"
RUN echo "NPM VERSION: $(npm -v)"
RUN if [ -z "${NPM_VER}" ]; then echo "using default npm version"; else npm install -g npm@$NPM_VER; echo "Install npm@$NPM_VER "; fi


# install heroku cli
# RUN curl https://cli-assets.heroku.com/install.sh | sh
RUN npm install -g --force heroku firebase-tools

# install docker and bash tool
RUN apk add --no-cache docker bash
# RUN cd ~ && wget https://releases.hashicorp.com/terraform/0.14.5/terraform_0.14.5_linux_amd64.zip
# RUN cd ~ && unzip terraform_0.14.5_linux_amd64.zip && mv terraform /usr/local/bin/

# add some helper scripts
RUN mkdir -p /opt/scripts
COPY ./scripts /opt/scripts
RUN chmod -R +x /opt/scripts
ENV PATH="/opt/scripts/aws:${PATH}"

ENTRYPOINT []