# AWS CLI tool docker image

## Build docker image

### Build Arguments

NODE_VER(**required**):

which node version will be used in this image. this parameter will add to the image tag prefix of alpine-like `node:16-alpine` (available tags: [Node Docker Images](https://hub.docker.com/_/node))

NPM_VER(optional):

which npm version will be used in this image.

### Build Command

`docker build --no-cache --build-arg NPM_VER=6.13.4 --build-arg NODE_VER=lts -t "public.ecr.aws/f2k1d6n4/docker-aws-cli:latest" .`

### Drone deploy

Using the promote, input the target (cloud be **node14**, **node16**, **latest**, **test**) and Parameters (**NODE_VER**, **NPM_VER**) then click the deploy button. Drone CI will build the docker image and push it to AWS public docker registry `public.ecr.aws/f2k1d6n4/docker-aws-cli:${target}`

### Help scripts

#### checkEcrRepositoryExist

> Usage: checkEcrRepositoryExist <repository-name> [Additional AWS CLI parameters...]
> Return: "true" | "false"

#### updateLambdaAlias

> Usage: updateLambdaAlias <FunctionName> <AliasName> <TargetVersion> [Additional AWS CLI parameters...]
> Return: None
